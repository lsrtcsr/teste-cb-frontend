import react from 'react';
import {useState} from 'react'
import './loginIndex.css';

//Images
import imagemDeFundo from '../../Images/Login/imagemDeFundo.jpg';
import logo from '../../Images/Login/logo.png';

//Routes 
import { useHistory } from "react-router-dom";
    
function balaoDeAviso(pedidos){
    return(<div className="balaoAviso">
        <div className="parteEsquerda">
            <span>{pedidos}</span>
            <span>
                novo pedido
            </span>
        </div>
        <div className="parteDireita">
            <p>
                Por favor faça o login para ver o pedido e ter acesso a receita com o modo de preparo
            </p>
        </div>
    </div>);
}

function Login(){
    const history = useHistory();
    const[pedidos, setPedidos] = useState(1);

    const[userName, setUsername] = useState('');
    const[password, setPassword] = useState('');
 
    function logar(){
    
        if(userName == process.env.REACT_APP_LOGIN_USER && password == process.env.REACT_APP_LOGIN_PASSWORD){
            history.push('/main')
            
        }else{
            console.log(process.env.REACT_APP_LOGIN_USER )
        }
    
    }

    return(
    <div className="fundo">
        <div className="Imagemfundo">
        </div>
            <form className='formLogin'>
                {pedidos > 0 ?  balaoDeAviso(pedidos) : null}
                <div className="logo"> 
                    <div className="logoBorder">
                        <img className="logoImg" src={logo}/>
                        <p className="logoTexto">RESTAURANTE</p>
                    </div>
                </div>
                
                <div className="inputs">
                    <input type='text' placeholder="Usuario" className="usernameInput" onChange={(e) => {setUsername(e.target.value)}}></input>
                    <input type='password' placeholder="Senha" className="passwordInput" onChange={(e) => {setPassword(e.target.value)}}></input>
                    <button className="submitBtn" onClick={() => logar()}>Acessar</button>
                </div>
            </form>
        
    </div>);
}

export default Login;