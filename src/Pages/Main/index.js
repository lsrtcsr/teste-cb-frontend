import react from 'react';
import {useState} from 'react'
import './mainIndex.css';

//Images
import imagemDeFundo from '../../Images/Login/imagemDeFundo.jpg';
import logoImg from '../../Images/Main/logo.png';
import chapeu from '../../Images/Main/Chapeu.png';
import chapeuSelecionado from '../../Images/Main/ChapeuSelecionado.png';
import imagemReceita from '../../Images/Main/arroz.jpg'

//Routes 
import { useHistory } from "react-router-dom";


function Main(){
    const history = useHistory();
    const[cards, setCardList] = useState([
    {
        imageUrl:'../../Images/Main/arroz.jpg',
        nome:'Arroz de Mariscos para 2 pessoas', 
        descricao:'Arroz com camarão, lula, peixe, lagosta e mexilhão, refogado com pimentões e cebola juliene, temperos e um leve toque de açafrão. Servidos na paellera. Rico em sabor e apresentação',
        horario:'2021-05-29 19:32:00'},
    {   
        imageUrl:'../../Images/Main/arroz.jpg',
        nome:'Arroz de Mariscos para 2 pessoas', 
        descricao:'Arroz com camarão, lula, peixe, lagosta e mexilhão, refogado com pimentões e cebola juliene, temperos e um leve toque de açafrão. Servidos na paellera. Rico em sabor e apresentação',
        horario:'2021-05-29 19:32:00'}
    ]);

    function acessarReceita(){
        history.push('/receita-preparo-detalhe')
    
    }

    function retornarCard(info){
        return(
        <div className="mainCard">
            <div className="mainCardPart1">
                <div className="mainImageDiv"/>
                
                <div className="mainCardInfo">
                    <h2 style={{marginTop:"0%"}}>
                        {info.nome}
                    </h2>
                    <p>
                        {info.descricao}
                    </p>
                </div>
            </div>
            <div className="mainCardPart2">
                <div className="mainCardButtons">
                    <div className="mainCardRadius mainCardFinalizado">
                        <h3 style={{textAlign:'center', fontSize:'17px'}}>Prato Finalizado</h3>
                    </div>
                    <div className="mainCardRadius mainCardTime">
                        <h3 style={{textAlign:'center', fontSize:'17px', marginBottom:'0'}}>Hoje</h3>
                        <h3 style={{textAlign:'center', fontSize:'17px', margin:'0'}}>19:33</h3>
                    </div>
                    <div className="mainCardRadius mainCardButton" onClick={() => acessarReceita()}>
                        <h3  style={{textAlign:'center', fontSize:'17px', marginBottom:'0'}}>Ver</h3>
                        <h3  style={{textAlign:'center', fontSize:'17px', margin:'0'}}>Receita</h3>
                    </div>
                </div>
            </div>
        </div>)
    }
    return(<div>
            <header className="mainHeaderBackground">
                <div className="mainLogo">
                    <div className="mainLogoImg"/>
                </div>
                <div className="mainInputDiv">
                    <input className="mainHeaderInput" placeholder="Buscar Receita..."/>
                </div>
                <div className="mainMenu">
                    <div className="mainMenuItem" style={{backgroundColor:'white'}}>
                        <img className="mainMenuItemImagem" src={chapeuSelecionado}/>
                        <p style={{color:'#FA771D' }} className="mainMenuTexto">Pedidos</p>
                    </div>
                    <div className="mainMenuItem">
                        <img className="mainMenuItemImagem" src={chapeu}/>
                        <p style={{color:'white'}} className="mainMenuTexto">Receita</p>
                    </div>
                    <div className="mainMenuItem">
                        <img className="mainMenuItemImagem" src={chapeu}/>
                        <p style={{color:'white'}} className="mainMenuTexto">Sair</p>
                    </div>
                </div>
            </header>

            <div className="mainSubHeader">
                <p className="mainSubHeaderTexto">Últimos Pedidos</p>
            </div>

            <div className="mainCardList">
                {cards.map((e) => {
                   return retornarCard(e);
                })}
            </div>
        </div>);
}

export default Main;