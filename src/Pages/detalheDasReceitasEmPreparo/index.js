import react from 'react';
import {useState} from 'react'

import './detalhesEmPreparo.css';

//Images
import imagemDeFundo from '../../Images/Login/imagemDeFundo.jpg';
import logoImg from '../../Images/Main/logo.png';
import Arrow from '../../Images/emPreparoDetalhe/Arrow.png';
import imagemReceita from '../../Images/Main/arroz.jpg'

//Routes 
import { useHistory } from "react-router-dom";

function ReceitaDetalhe(){

    const history = useHistory();

    const[receita, setReceita] = useState(
        {
            imageUrl:'../../Images/Main/arroz.jpg',
            nome:'Arroz de Mariscos para 2 pessoas', 
            descricao:'Arroz com camarão, lula, peixe, lagosta e mexilhão, refogado com pimentões e cebola juliene, temperos e um leve toque de açafrão. Servidos na paellera. Rico em sabor e apresentação',
            horario:'2021-05-29 19:32:00',
            ingredientes:['1 cebola', '2 dentes de alho', '3 colheres de sopa de Azeite', '4 tomates', '2 Pitadas de Sal', '1 embalagem de marisco(mistura)','1 embalagem de camarão inteiro congelado', '1 chávena de arroz', '1 porção de coentros'],
            modoDePreparo:['Faça um refogado com azeite a cebola e os dentes de alho bem picados esmague os tomates maduros sem pele e junte ao refogado. Tempere com sal. Deixe "namorar" durante alguns minutos',
            'Deixe a descongelar a embalagem de cocktail de marisco e delicias do mar, retire-as também do congelador e ponha-as de parte',
            'Junte os mariscos (as delícias ficam para mais tarde) ao refogado e mex. Com o lume brando, tape o tacho e deixe "namorar" durante 15 minutos',
            'Apague D lume, junto as delicias cortadas em cubinhos e povilho com coentros picados, Sirva em seguida',
            'Depois temos o Arroz de Marisco Tradicional que leva todo tipo de marisco, desde sapateira, lagosta, mexilhões e etc.']
        });

        function retornarCardIngredientes(info){
        
            return(
            <div className="emPreparoIngredientCard">
                <div className="emPreparoCheckBoxIngredientes" >

                </div>
                <div className="emPreparoNome">
                    <p className="emPreparoIngredientesNome">
                        {info}
                    </p>
                    
                </div>
                
            </div>)
        
        }

        function retornarCardModo(info,index){
        
            return(
            <div className="emPreparoIngredientCard">
               
                <div className="emPreparoCheckBoxIngredientes">
                    
                </div>
               
                <div className="emPreparoModoNome">
                    <h3 className="emPreparoIngredientesModoNome">Passo {index}</h3>
                    <p className="emPreparoIngredientesModoNomeDescription">
                        {info}
                    </p>
                    
                </div>
                
            </div>)
        
        }



    return(<div className="emPreparoTelaInteira">
            <div className="emPreparoReceita">
                   

                    <div className="emPreparoImgDaReceita">
                        <div className="emPreparoNomeEDescricao">
                            <h2 style={{marginTop:"0%"}}>
                                {receita.nome}
                            </h2>
                            <span>
                                {receita.descricao}
                            </span>
                        </div>
                    </div>

                    <div className="emPreparoVoltar" onClick={() => history.push('/main')}>
                        <img src={Arrow} className="emPreparoArrow"/>
                        <span className="emPreparoVolarTexto">Voltar</span>
                    </div>
            
                    <div className="emPreparoTempo">
                        <div className="emPreparoRelogioIcone"></div>
                        <div className="emPreparoTempoTextos">
                            <p className="emPreparoTempoTexto1">Tempo de Preparo</p>
                            <p className="emPreparoTempoTexto2">25 Minutos</p>
                        </div>
                    </div>
            </div>
            
            <div className="emPreparoIngredientes">
                <p className="emPreparoIngredientesTitulo">Ingredientes</p>
                {receita.ingredientes.map((e) =>{
                    return retornarCardIngredientes(e);
                })}
            </div>

            <div className="emPreparoModo">
                <p className="emPreparoIngredientesTitulo">Modo de Preparo</p>
                {receita.modoDePreparo.map((e, index) =>{
                    return retornarCardModo(e, index+1);
                })}
            </div>

            <div className="emPreparoFooter">
                <div className="emPreparoSubFooter">
                    <div className="emPreparoSubFooterPart1">
                        <span className="emPreparoSubFooterText">Status 0% pronto e 0 minutos utilizados</span>
                        <div className="progressBarEmpty">
                            <div className="progressBarPercentage" style={{ width:'10%'}}></div>
                        </div>
                    </div>
                    <div className="emPreparoSubFooterPart2">
                        <button className="emPreparoSubFooterButton">Iniciar Preparação</button>
                    </div>
                </div>
            </div>
        </div>
        );
}

export default ReceitaDetalhe;