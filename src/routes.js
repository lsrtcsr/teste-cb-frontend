import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';

//PAGES
import Login from './Pages/Login';
import Main from './Pages/Main';
import ReceitaDetalhe from './Pages/detalheDasReceitasEmPreparo';


function Routes(){
    return(

        <BrowserRouter>
            <Switch>
                <Route path="/" exact component={Login}></Route>
                <Route path="/main" component={Main}></Route>
                <Route path="/receita-preparo-detalhe" component={ReceitaDetalhe}></Route>
            </Switch>
        </BrowserRouter>

    );
};

export default Routes;